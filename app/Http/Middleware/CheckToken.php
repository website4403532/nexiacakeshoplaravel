<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class CheckToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // $need_access_token = [
        //     'admin'
        // ];

        // $url = $request->path();
        // $parsedUrl = "";
        // $reverseUrl = strrev($url);
        // $index = -1;

        // for($i = 0; $i< strlen($reverseUrl); $i++){
        //     if($reverseUrl[$i] == '/'){
        //         $index = $i;
        //         break;
        //     }
            
        // }
        // $parsedUrl = strrev(substr($reverseUrl, $index+1));

        // if($index == -1) $parsedUrl = $url;

        // if(in_array($parsedUrl, $need_access_token)){
        //     if((Session::get('username') != "guest"))
        //         return redirect('/admin/login');
        // }

        return $next($request);
        
    }
}
