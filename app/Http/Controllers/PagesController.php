<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\contact;
use DB;

class PagesController extends Controller
{
    public function index(){
        return view('welcome');
    }

    public function admin(){
        $contact = contact::all();
        return view ('admin.home', ['contacts' => $contact]);
    }
}
