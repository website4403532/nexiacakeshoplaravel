<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\admin;
use DB;
use Session;

class LoginController extends Controller
{
    public function postlogin(Request $request){
        $user = new admin();

        $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);

        $exe = $user->where('username', $request->username)
                    ->first();
        if($exe == NULL){
            return redirect('/admin/login')->with('failed','User not Found!');
        }
        if($request->password != $exe->password){
            return redirect('/admin/login')->with('failed','Incorrect Password');
        }
        
        Session::put('username',$exe->username);
        return redirect ('/admin/');
    }

    public function logout(){
        Session::flush();
        return redirect('/admin/login');
    }
}