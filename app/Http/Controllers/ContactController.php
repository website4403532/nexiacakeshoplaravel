<?php

namespace App\Http\Controllers;

use App\contact;
use DB;
use Illuminate\Http\Request;

class ContactController extends Controller
{
    public function store(Request $request){
        $request->validate([
            'fullname' => 'required',
            'email' => 'required',
            'phonenumber' => 'required|numeric',
            'message' => 'required|max:255'
        ]);

        Contact::create([
            'fullname' => $request["fullname"], 
            'email' => $request["email"], 
            'phonenumber' => $request["phonenumber"], 
            'message' => $request["message"]
        ]);
        return redirect('/#contact')->with('success','Data has been sent, we will contact you soon!');
    }

    public function show(contact $contact){
        return view ('admin.detail', compact('contact'));
    }

    public function destroy(contact $contact){
        Contact::destroy($contact->id);
        return redirect ('/admin')->with('success','Message has been Deleted!');
    }
}
