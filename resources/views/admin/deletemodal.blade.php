{{-- modal --}}
<div class="modal fade" id="modal-delete-{{ $contact->id }}" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header bg-danger">
            <h5 class="modal-title text-white">Delete Message</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <p>Are you sure want to delete message from <span>{{ $contact->fullname }}</span>?</p>
        </div>
        <div class="modal-footer">
            <form action="{{ $contact->id }}" method="POST" class="d-inline">
                @method('delete')
                @csrf
                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
        </div>
    </div>
</div>