@extends('layout.templateadmin')
@section('title'.'Admin Center')
@section('isi')
<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-8">
        <h1 class="text-center">Admin Manager</h1>
        @if(session('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            {{ session('success') }}
        </div>
        @endif
        <table class="table table-striped">
            <thead class="thead-dark">
                <tr>
                    <th scope="col">No.</th>
                    <th scope="col">Fullname</th>
                    <th scope="col">E-mail</th>
                    <th scope="col">Phone Number</th>
                    <th scope="col">Action</th>
                </tr>
            </thead>
            <tbody>
                @foreach($contacts as $contact)
                <tr>
                    <th scope="row">{{$loop->iteration}}</th>
                    <td>{{$contact->fullname}}</td>
                    <td>{{$contact->email}}</td>
                    <td>{{$contact->phonenumber}}</td>
                    <td>
                        <a href="/admin/detail-message/{{$contact->id}}"><button class="btn btn-primary"><i class="fas fa-eye"></i></button></a>
                        {{-- <a href="javascript:void(0)" data-toggle="modal" data-target="#modal-delete-{{ $contact->id }}" class="btn btn-danger"><i class="fas fa-trash"></i></a> --}}
                    </td>
                </tr>
                @include('admin/deletemodal')
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="col-md-2"></div>
</div>
@endsection