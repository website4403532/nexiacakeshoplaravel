@extends('layout.navbar')
@section('title'.'Welcome to Nexia Cake Shop')
@section('isi')
<div class="container-fluid">
{{-- Home --}}
    <div id="home">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <h1 class="text-center">Welcome to Nexia Cake Shop</h1>
                <ul class="rslides">
                    <li><img src="{{ asset('img/cake4.jpg')}}" alt="" srcset=""></li>
                    <li><img src="{{ asset('img/cake3.jpg')}}" alt="" srcset=""></li>
                    <li><img src="{{ asset('img/cake2.jpg')}}" alt="" srcset=""></li>
                </ul>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>
{{-- About --}}
    <div id="about">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <hr/>
                <h1 class="text-center">About US</h1>
                <div class="row">
                    <div class="col-md-6">
                        <img src="{{ asset('img/cake1.jpg')}}" alt="Online Shop Logo" class="img-thumbnail">
                    </div>
                    <div class="col-md-6">
                        <p>Nexia Cake Shop is a Cake Shop located in Jakarta, Indoensia. We use the best ingredients and recipes. Proven in 2 years. Our customers very happy of our cakes. Currently, we only accept online orders.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>    
    </div>

{{-- Products --}}
    <hr/>
    <div id="product">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <h1 class="text-center">Our Products</h1>
                <div class="row">
                    <div class="col-md-4">
                        <div class="card" style="width: 18rem;">
                            <div class="card-top">
                                <img src="{{ asset('img/cheesecake.jpg')}}" alt="" class="card-img-top img-thumbnail">
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">Cheesecake</h5>
                                <p class="card-text">Cake that made from Cheese</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card" style="width: 18rem;">
                            <div class="card-top">
                                <img src="{{ asset('img/operacake.jpg')}}" alt="" class="card-img-top img-thumbnail">
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">Opera Cake</h5>
                                <p class="card-text">Opera in Cake? Why not?</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card" style="width: 18rem;">
                            <div class="card-top">
                                <img src="{{ asset('img/oreocheesecake.jpg')}}" alt="" class="card-img-top img-thumbnail">
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">Oweo Cheesecake</h5>
                                <p class="card-text">Everyone loves Oweo</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>

    {{-- Review --}}
    <hr/>
    <div id="review">
        <div class="row">
            <div class="col-md-2"></div>
            <div class="col-md-8">
                <h1 class="text-center">Our Review</h1>
                <div class="embed-responsive embed-responsive-16by9">
                    <iframe class="embed-responsive-item" width="560" height="315" src="https://www.youtube.com/embed/dQw4w9WgXcQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
            <div class="col-md-2"></div>
        </div>
    </div>

    {{-- Contact --}}
    <hr/>
    <div id="contact">
        <div class="row">
            <div class="col-md-1"></div>
            <div class="col-md-10">
                <h1 class="text-center">How to Reach US</h1>
                <div class="row">
                    <div class="col-md-6">
                        <h5 class="text-center">Simple, please fill this form, we will reach you</h5>
                        @if(session('success'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ session('success') }}
                        </div>
                        @endif
                        <form action="/contactpost" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="">Fullname</label>
                                <input type="text" name="fullname" id="fullname" class="form-control @error('fullname') is-invalid @enderror" placeholder="Input Fullname" value="{{ old('fullname') }}">
                                @error('fullname')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="">E-mail</label>
                                <input type="email" name="email" id="email" class="form-control @error('email') is-invalid @enderror" placeholder="Input E-mail" value="{{ old('email') }}">
                                @error('email')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="">Phone Number</label>
                                <input type="text" name="phonenumber" id="phonenumber" class="form-control  @error('phonenumber') is-invalid @enderror" placeholder="Input Phone Number" value="{{ old('phonenumber') }}">
                                @error('phonenumber')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="">Your Message</label>
                                <textarea name="message" id="message" class="form-control @error('message') is-invalid @enderror" cols="5" rows="8" placeholder="Input Message" value="{{ old('message') }}"></textarea>
                                @error('message')
                                <div class="invalid-feedback">
                                    {{ $message }}
                                </div>
                                @enderror
                            </div>
                            <input type="submit" value="Submit" class="btn btn-primary btn-lg btn-block">
                        </form>
                    </div>
                    <div class="col-md-6">
                        <h5 class="text-center">Or, Contact us below</h5>
                        <table class="table table-borderless">
                            <tbody>
                                <tr>
                                    <td class="float-left"><i class="fas fa-phone"></i></td>
                                    <td class="float-left">0218192110</td>
                                </tr>
                                <tr>
                                    <td class="float-left"><i class="fab fa-whatsapp"></i></td>
                                    <td class="float-left">08129101020</td>
                                </tr>
                                <tr>
                                    <td class="float-left"><i class="fas fa-envelope"></i></td>
                                    <td class="float-left">admin@nexiacakeshop.com</td>
                                </tr>
                                <tr>
                                    <td class="float-left"><i class="fab fa-instagram"></i></td>
                                    <td class="float-left">nexiacakeshop</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-1"></div>
        </div>
    </div>
</div>
@endsection