<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix' => '/'], function () {
    Route::get('/','PagesController@index');
    Route::post('/contactpost','ContactController@store');

});

Route::group(['prefix' => 'admin'], function () {
    Route::get('/','PagesController@admin');

    // Login
    Route::get('/login','PagesController@admin');
    Route::post('/postlogin','LoginController@postlogin');

    // Detail Message
    Route::get('/detail-message/{contact}','ContactController@show');

    // Delete Message
    Route::delete('/detail-message/{contact}','ContactController@destroy');

    // Logout
    Route::get('/logout','LoginController@logout');

});